#include "ros/ros.h"
#include "std_msgs/String.h"
#include <geometry_msgs/PoseStamped.h>

#include <sstream>


int main(int argc, char **argv)
{
 
  ros::init(argc, argv, "navGoalPublisher");

  
  ros::NodeHandle n;
  //bool latch = true;

  
  ros::Publisher goal_pub = n.advertise<geometry_msgs::PoseStamped>("move_base_simple/goal", 10);

  ros::Rate loop_rate(10);

  while(ros::ok()){

    geometry_msgs::PoseStamped poseStamped;
    poseStamped.header.frame_id="world";
    poseStamped.header.stamp = ros::Time::now();

    poseStamped.pose.position.x = 1.0; 
    poseStamped.pose.position.y = 1.0;
    poseStamped.pose.position.z = 1.0;

    poseStamped.pose.orientation.x = 0.0;
    poseStamped.pose.orientation.y = 0.0;
    poseStamped.pose.orientation.z = 0.0;
    poseStamped.pose.orientation.w = 0.0;


    //while(goal_pub.getNumSubscribers() == 0){
        //loop_rate.sleep();
        //std::cout<<"waiting";
   // }
    goal_pub.publish(poseStamped);
    //ros::spinOnce();

    loop_rate.sleep();

  }
  

  return 0;
}